# Pourquoi ArchiveExtractor

L'idée est de pouvoir récupérer assez facilement des données depuis l'archivage vers un script python.
Le passage le plus simple identifié est de passer par les devices extractor.

ArchiveExtractor contourne des manques de fonctionnalités ou bugs repérés sur les Extractors.
Le code du module a été rédigé au moment du besoin, ainsi il est possible que ces éléments aient été ajouté/corrigé depuis.

## Fonctionnalités

### Dates moins précises, ou delta temps

Via le module, il est possible de spécifier des dates de façon moins précises.
ie "%Y-%m-%d-%H"  "%Y-%m".
Il est également possible de spécifier un delta de temps qui sera appliqué au regard de l'autre date précisée.

voir fonction `Amenities._dateparse`

### Données sous forme de pandas.DataSeries

Ce format est très pratique pour les timeseries.
L'index est sous forme de datetime (plutôt que des timestamps en milliseconds)

Il est possible de récupérer une liste d'attributs, voir un dict permettant un alias rapide pour le reste du script.

### Recherche des attributs archivés

Une fonction permet de réaliser une recherche parmis les valeurs archivés et retourne une liste python.
Pratique pour extraire par exemple toutes les positions de BPM en C14, sans les lister tous à la main.

## Contournements

### Capacité d'extraction

Afin de ne pas surcharger le device, les commandes d'extraction sont envoyés en limitant dans la requête le nombre de données récupérés.
Elle sont aggrégées par la suite en mémoire avant d'être rendues à l'utilisateur.

Voir fonction `Amenities._chunkerize`

### DevFailed sur command

Le device extractor peut parfois retourner un DevFailed sur une commande d'extraction.
ArchiveExtractor réalise alors une nouvelle tentative.

Voir fonction `Amenities._cmd_with_retry`

### Cast booleen

Le device extractor rend les booléen sous forme de String.
Cast en python bool

voir fonction `Amenities._cast_bool`


### GetNearestValue ne marche pas sur des spectrum

ArchiveExtractor réalise une recherche autour de la date visée

voir fonction `Core._extract_vector`

### Les fonctions GetAttData utilisent l'history d'attribut dynamique

NB: pour récupérer des spectrum, on ne peut utiliser la command `ExtractBetweenDates`

Pour faciliter la récupération, ArchiveExtractor récupère l'historique et supprime l'attribut ensuite.
Il récupère aussi la date pour recréer l'index du pandas.DataFrame retourné.




