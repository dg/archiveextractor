# ArchiveExtractor
This module brings some functions to extract data from HDB/TDB.

## Quick example
Usage example, with an ipython prompt

```python
In [1]: import ArchiveExtractor as AE

In [2]: # Looking for an attribute in HDB
   ...: AE.findattr("ans/dg/*dcct*")
Out[2]: 
['ANS/DG/DCCT-CTRL/State',
 'ANS/DG/DCCT-CTRL/Status',
 'ANS/DG/DCCT-CTRL/current',
 'ANS/DG/DCCT-CTRL/dose',
 'ANS/DG/DCCT-CTRL/lifeTime',
 'ANS/DG/DCCT-CTRL/lifeTimeErr']

In [3]: # Get data between two dates, this return a pandas.Dataframe object
   ...: AE.extract('ANS/DG/DCCT-CTRL/current', '2021-12-13', '2021-12-13-12:00')
INFO:Perform ExtractBetweenDates (ans/dg/dcct-ctrl/current, 2021-12-13 00:00:00, 2021-12-13 12:00:00)
Out[3]: 
2021-12-13 00:00:00    450.993568
2021-12-13 00:00:01    450.981979
2021-12-13 00:00:02    450.971455
2021-12-13 00:00:03    450.950542
2021-12-13 00:00:04    450.939951
                          ...    
2021-12-13 11:59:56     15.004553
2021-12-13 11:59:57     15.004243
2021-12-13 11:59:58     15.004942
2021-12-13 11:59:59     15.004878
2021-12-13 12:00:00     15.005410
Length: 42725, dtype: float64

```
## Main function help


```
Signature: AE.extract(attr, date1, date2=None, method='nearest', db='H')
Docstring:
Access function to perform extraction between date1 and date2.
Can extract one or several attributes.
date1 and date2 can be both exact date, or one of two can be a time interval that will be taken relative to the other.


Parameters:
-----------
attr: string, list, dict
    Attribute(s) to extract.
    If string, extract the given attribute, returning a pandas.Series.
    If list, extract attributes and return a list of pandas.Series.
    If a dict, extract attributes and return a dict of pandas.Series with same keys.

date1, date2: string, datetime.datetime, datetime.timedelta, None
    Exact date, or duration relative to date2.
    If string, it will be parsed.
    A start date can be given with string format '%Y-%m-%d-%H:%M:%S' or less precise (ie '2021-02', '2022-11-03' '2022-05-10-21:00'.i..).
    A duration can be given with string format 'Xu' where X is a number and u is a unit in ('m':minutes, 'h':hours, 'd':days, 'M':months)
    A datetime.datetime object or datetime.timedelta object will be used as is.
    date2 can be None. In that case it is replaced by the current time.

method: str
    Method of extraction
        'nearest': Retrieve nearest value of date1, date2 is ignored.
        'between': Retrive data between date1 and date2.

db: str
    Which database to look in, 'H' or 'T'.

```

## For developpers

### Contribute

There is a simple test directory with only a manual test scipt.
Please run and check it before proposing merge.

