
# Recherche des rendements d'injection sur les 3 derniers jours

#%% Récupération des données
import ArchiveExtractor as AE
# On récupère tous les attributs de rendement stockés sur injection

lattr = AE.findattr("*dg*rendement/efficiency*arch")

# préparation d'un dict, récupération que de la dénomination utile pour la clé (ie LT2_LPM)
dattr = {s[-12:-5]:s for s in lattr}

# On ajoute le courant aussi
dattr["current"]="ans/dg/dcct-ctrl/current"

data = AE.extract(dattr, "3d", method="between")

#%% Affichage des données

import matplotlib.pyplot as plt

fig, axes = plt.subplots(3,1, sharex=True, figsize=(12,6))


for tinj, ax in zip(
        ("SPM", "LPM"),
        axes[1:],
        ):
    
    ax.set_title("Efficiencies "+tinj)
    
    for mach, col in zip(
            ('ANS', 'Boo', 'LT2', 'LT1'),
            ('C0', 'C2', 'C1', 'C3'),
            ):
        
        ax.plot(data[mach+"_"+tinj],
                color=col,
                label=mach+"_"+tinj,
                linestyle="", alpha=0.5, markersize=3, marker="o",
                )
        
    ax.set_ylim(0,110)
    ax.legend()
    ax.grid(alpha=.3)


axes[0].plot(data["current"])
axes[0].set_title("Beam current")
axes[0].grid(alpha=.3)
