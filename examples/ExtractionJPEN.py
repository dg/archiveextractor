
import ArchiveExtractor as AE

import pandas as pd


#%% Récupération des pression des JPEN de l'ANS sur le mois de décembre 2023


ljpen = AE.findattr("ans-c*vi*jpen*pressure")

data = AE.extract(ljpen, "2023-01", "1M", method='between')


#%% Réduction des données à l'heure par moyenne et concatenation

datacat= pd.concat([d.resample("1h").mean() for d in data], axis=1)

# Ecriture dans un fichier CSV dans le répertoire courant
datacat.to_csv("ANS_JPEN.csv")
