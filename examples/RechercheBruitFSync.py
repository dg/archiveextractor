
import ArchiveExtractor as AE

import matplotlib.pyplot as plt
import matplotlib.ticker as mtk
import matplotlib.dates as mdates


# %% Extraction depuis la HDB

data = AE.extract({
"current":"ans/dg/dcct-ctrl/current",
"xint_1kHz":'ANS-C07/DG/MESURESPECTRE/X_1kHz',
"xint_10kHz":'ANS-C07/DG/MESURESPECTRE/X_10kHz',
"zint_1kHz":'ANS-C07/DG/MESURESPECTRE/z_1kHz',
"zint_10kHz":'ANS-C07/DG/MESURESPECTRE/z_10kHz',
"fofb":'ANS/DG/FOFB-WATCHER/FOFBRunning_x',
"fofb_old":'ANS/DG/FOFB-MANAGER/xFofbRunning',
"temperature":'T7/GTC/Circuit_30C/temperature_retour',
}, "2024-01-24-20:00", "2024-01-29", method="between", db="H")

#%% Affichage via Matplotlib

fig, axes = plt.subplots(3,1, sharex=True, figsize=(12,8))

axes[0].set_title("Courant et FOFB running")
axes[0].plot(data["current"], label="courant (mA)")


axes[0].plot(data["fofb"]*400, drawstyle="steps-post", label="fofb (new)")
axes[0].plot(data["fofb_old"]*400, drawstyle="steps-post", label="fofb (old)")

axes[1].set_title("Bruit integré")
axes[1].plot(data["xint_1kHz"], label="X 1kHz")
axes[1].plot(data["xint_10kHz"], label="X 10kHz")
axes[1].plot(data["zint_1kHz"], label="Y 1kHz")
axes[1].plot(data["zint_10kHz"], label="Y 10kHz")



axes[1].set_ylabel("µm")

axes[2].set_title("Température")
axes[2].plot(data["temperature"], label="retour 30°")

for ax in axes:
    ax.legend()
    ax.grid(alpha=.3)

locator = mdates.AutoDateLocator()
ax.xaxis.set_major_locator(locator)
ax.xaxis.set_major_formatter(mdates.ConciseDateFormatter(locator))
fig.tight_layout()
