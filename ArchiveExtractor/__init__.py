"""
Python module for extracting data from Archive Extractor Tango Device.

Example scripts available on GitLab: https://gitlab.synchrotron-soleil.fr/dg/archiveextractor/-/tree/main/examples

Main functions
    * findattr
        Look into the list of archived attributes for matching names.
    * extract
        Entry point function for data extraction.

Quick usage in ipython shell:

    In [1]: import ArchiveExtractor as AE
    In [2]: AE.findattr("ans/dg/*dcct*")
    In [3]: AE.extract('ANS/DG/DCCT-CTRL/current', '2021-12-13', '2021-12-13-12:00', method='between')

"""

# NB: Also change version number in setup.py
__version__ = "2.4"

__all__ = ["Access", ]

##########################################################################
###                  Module private variables                          ###
##########################################################################
# Tuple of extractor for HDB and TDB
_Extractors = (None, None)

# Tuple for attribute tables
_AttrTables = (None, None)

##########################################################################
###               Functions in Access are entry points                 ###
##########################################################################
from .Access import *


## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
## Initialize on import
## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ##
init()
