#!/usr/bin/env python

from distutils.core import setup

setup(name='ArchiveExtractor',
      version='2.4', # NB Also change version number in __init__.py
      description='Python module for extracting data from Archive Extractor Tango Device.',
      url='https://gitlab.synchrotron-soleil.fr/dg/archiveextractor',
      license="GNU-GPL-v3",
      author='Romain BRONÈS',
      author_email='romain.brones@synchrotron-soleil.fr',
      packages=['ArchiveExtractor',],
      install_requires = ['pytango>=9.2', 'numpy>=1.15', 'pandas>=1.1'],
     )
